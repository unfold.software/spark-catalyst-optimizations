import fileinput
import re

def main():
    graphs_with_title = []
    current_title = None
    current_lines = []
    left_width = 0

    for l in fileinput.input():
        line = l.strip()
        if line.startsWith("="):
            if current_title is not None:
                parse_graph(current_title, current_lines)
            current_title = line
            current_lines = []
        
        if line.startsWith("!"):
            current_lines.append(line[1:])

    parse_graph(current_title, current_lines)

def parse_graph(title, lines):
    length = len(re.search('(.*\s{3,}).*', lines[0]).group(1))
    before = [line[:length].rstrip() for line in lines if line[:length].rstrip()]
    after = [line[length:] for line in lines if line[length:]]

    before_tree = generate_tree(before)
    after_tree = generate_tree(after)

    print_graph(before_graph)
    print_graph(after_graph)

def print_graph(tree):
    print("graph BT")
    print_tree(tree)
    print("")

def print_tree(tree):
    print(tree[2] + "(\"" + tree[0] + "\")")
    if len(tree[2]) > 1:
        print(tree[2] + "-->" + tree[2][:-1])
    for subtree in tree[1]:
        print_tree(subtree)

def generate_tree(lines):
    lines_with_indent = [assign_indent(line) for line in lines]
    path = []
    for lwi in lines_with_indent:
        path = path[:lwi[0]] #cut the path to the indent
        tmp_element = tree
        for step in path:
            tmp_element = tmp_element[1][step]
        path.append(len(tmp_element[1]))
        tmp_element[1].append((lwi[1], [], "".join(map(lambda e: str(e), path))))

    return(tree[1][0])

def assign_indent(line):
    print(line)
    regex = re.search('(\W*)(\w.*)', line)
    return(len(regex.group(1)), regex.group(2))

if __name__ == "__main__":
    main()
