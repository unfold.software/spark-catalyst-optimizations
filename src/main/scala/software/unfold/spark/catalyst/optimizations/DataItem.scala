package software.unfold.spark.catalyst.optimizations

case class DataItem(id: Int, name: String, value: Long)
