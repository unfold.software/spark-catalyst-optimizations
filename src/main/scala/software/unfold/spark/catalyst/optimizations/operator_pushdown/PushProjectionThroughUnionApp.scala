package software.unfold.spark.catalyst.optimizations.operator_pushdown

import software.unfold.spark.catalyst.optimizations.SparkApp

object PushProjectionThroughUnionApp extends SparkApp {

  import spark.implicits._
  spark.sparkContext.setLogLevel("TRACE")

  val size = 1000
  val dataset1 = generateDataset(size)
  val dataset2 = generateDataset(size, size)

  dataset1.union(dataset2).select('name.as[String]).explain(true)

  /*
=== Applying Rule org.apache.spark.sql.catalyst.optimizer.PushProjectionThroughUnion ===
!Project [name#4]                                  Union
!+- Union                                          :- Project [name#4]
!   :- LocalRelation [id#3, name#4, value#5L]      :  +- LocalRelation [id#3, name#4, value#5L]
!   +- LocalRelation [id#10, name#11, value#12L]   +- Project [name#11]
!                                                     +- LocalRelation [id#10, name#11, value#12L]
   */
}
