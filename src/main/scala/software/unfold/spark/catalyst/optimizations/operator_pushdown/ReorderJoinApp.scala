package software.unfold.spark.catalyst.optimizations.operator_pushdown

import software.unfold.spark.catalyst.optimizations.SparkApp

object ReorderJoinApp extends SparkApp {

  spark.sparkContext.setLogLevel("TRACE")

  val size = 1000
  val bigDataset1 = generateDataset(size)
  val bigDataset2 = generateDataset(size)
  val smallDataset = generateDataset(10)

  bigDataset1.crossJoin(bigDataset2).join(smallDataset).where(bigDataset1("id") === smallDataset("id")).explain(true)

  /*
=== Applying Rule org.apache.spark.sql.catalyst.optimizer.ReorderJoin ===
!Filter (id#3 = id#17)                                Project [id#3, name#4, value#5L, id#10, name#11, value#12L, id#17, name#18, value#19L]
!+- Join Inner                                        +- Join Cross
!   :- Join Cross                                        :- Join Inner, (id#3 = id#17)
    :  :- LocalRelation [id#3, name#4, value#5L]         :  :- LocalRelation [id#3, name#4, value#5L]
!   :  +- LocalRelation [id#10, name#11, value#12L]      :  +- LocalRelation [id#17, name#18, value#19L]
!   +- LocalRelation [id#17, name#18, value#19L]         +- LocalRelation [id#10, name#11, value#12L]
   */
}
