package software.unfold.spark.catalyst.optimizations

import org.apache.spark.sql.{Dataset, SparkSession}

trait SparkApp extends App {

  val spark = SparkSession.builder().appName("spark-app").master("local[*]").getOrCreate()

  def generateDataset(size: Int, start: Int = 0): Dataset[DataItem] = {
    import spark.implicits._
    Range(start, start+size).map(i => DataItem(i, s"$i", i.toLong)).toDS
  }
}
